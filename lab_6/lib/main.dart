import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          color: Colors.greenAccent,
        ),
        scaffoldBackgroundColor: const Color(0xFFFFFFFF),
        fontFamily: 'Poppins',
        textTheme: const TextTheme(
          headline6: TextStyle(
              fontSize: 30.0, fontFamily: 'Poppins', fontWeight: FontWeight.bold),
          bodyText2: TextStyle(
              fontSize: 20.0, fontFamily: 'Poppins', color: Colors.white),

        ),
      ),
      home: const MyHomePage(title: 'KOPIT SUPPLIES'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: ListView(
          padding: const EdgeInsets.all(10),
          children: <Widget>[
            Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
                color: Colors.black38 ,
              ),
              padding: const EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  const Text('Vitamin Supplies',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25.0,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w900)),
                  const Text('Stok : 17',
                      style: TextStyle(
                          color: Colors.white ,
                          fontSize: 15.0,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w900)),

                  const Text(''),
                  const Text('Vitamin yang akan didapatkan : Vitamin C, Vitamin D3, Zinc.',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 17.0,
                          fontFamily: 'Poppins')),
                  const Text(''),
                  const Text('Contact for Booking : +62 8532818219 or Kopit@gmail.com ', 
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 17.0,
                          fontFamily: 'Poppins')),
                  const Text(''),
                  ElevatedButton(
                    onPressed: () {},
                    child: const Text('Book Now'),
                    style: ElevatedButton.styleFrom(
                        primary: Colors.black12 ,
                        onPrimary: Colors.white,
                        textStyle: const TextStyle(fontSize: 14)),
                  ),
                ],
             
              ),
            ),
          ],
        ),
      ),
    );
  }
}