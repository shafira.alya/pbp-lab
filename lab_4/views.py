from django.shortcuts import render 
from django.http.response import HttpResponseRedirect
from .forms import NoteForm 
from lab_2.models import Note

def index(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    form = NoteForm()
    if request.method == 'POST':
        form = NoteForm(request.POST)
        form.save()

        if form.is_valid():
            form.save()
        return HttpResponseRedirect('/lab-4')

    response = {'form' : form}
    return render(request, 'lab4_form.html', response)


def note_list(request):
    response = {'note' : Note.objects.all().values()}
    return render(request, "lab4_note_list.html",response)