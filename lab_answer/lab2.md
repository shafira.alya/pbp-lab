
1. Apakah perbedaan antara JSON dan XML?

Sudah dijelaskan sebelumnya secara singkat pada dokumen soal Lab 2 mengenai definisi dari JSON dan XML. JSON (JavaScript Object Notation) dan XML (eXtensible Markup Language) adalah dua format umum pertukaran data dari berbagai sistem di website. Json adalah sebuah tools pertukaran data diantara berbagai platform. JSON mudah dibaca dan ditulis dan sebagian besar JSON digunakan untuk Asynchronous JavaScript (AJAX) serta pengembangan layanan web RESTful. Sedangkan, XML merupakan bahasa markup yang menggunakan struktur untuk merepresentasi item dari sebuah data. Mirip dengan HTML, XML memungkinkan pemrogram membuat tag sendiri. Dari penjabaran diatas sudah terlihat perbedaan dari keduanya, untuk lebih jelasnya akan dijelaskan berupa poin-poin. 

Berikut perbedaannya:
- Berdasarkan tipenya, JSON adalah bahasa meta dan XML adalah bahasa markup
- Berdasarkan kompleksitasnya, JSON lebih sederhana dalam pembacaan dan penulisannya, sedangkan XML lebih rumit dan sulit
- Orientasi pada JSON adalah datanya, sedangkan XML pada dokumennya
- Format ekstensi file JSON adalah diakhiri dengan .json, sedangkan XML adalah .xml


2. Apakah perbedaan antara HTML dan XML?
Sama halnya dengan XML yang sudah dijelaskan diatas, HTML merupakan bahasa markup yang digunakan untuk pembuatan halaman dan aplikasi dari web. HTML juga merupakan pemrograman yang digunakan untuk penerapan tata letak dan konvensi pemformatan ke dalam dokumen teks. Kesamaan keduanya, bahasa markup akan membuat website menjadi lebih interaktif dan dinamis. 

Untuk perbedaannya secara jelas, berikut poin-poin perbedaan dari keduanya: 
- HTML lebih berfokus pada penyajian data, sedangkan XML lebih berfokus pada transfer atau pengiriman sebuah data
- XML terdiri atas case sensitive, sedangkan XML terdiri atas case insensitive
- HTML juga lebih berfokus pada format dari tampilan data, sedangkan XML lebih kepada struktur serta konteksnya.
- Format ekstensi file HTML adalah diakhiri dengan .html, sedangkan XML adalah .xml

Dari kedua nomor tersebut, sebenarnya masih banyak perbedaan antara XML, JSON, dan HTML. Pada HTML dan XML, meskipun berbeda tetapi keduanya dapat melengkapi antar satu sama lain dalam penggunaannya.

Referensi :
1. https://blogs.masterweb.com/perbedaan-xml-dan-html/#Perbedaan_XML_dan_HTML 
2. https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html




