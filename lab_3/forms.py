from lab_1.models import Friend
from django import forms
from django.forms import widgets

class DateInput(forms.DateInput): # Membuat date field (calendar)
    input_type = 'date'

class FriendForm(forms.ModelForm):
    class Meta: 
        model = Friend
        fields = "__all__" #import
        widgets = {'dob': DateInput()} #import