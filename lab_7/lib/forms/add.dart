import 'package:flutter/material.dart';

class FormAdd extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormAddState();
  }
}

class FormAddState extends State<FormAdd> {
  String? nama = '';
  String? alamat = '';
  String? jumlah = '';
  String? nomor = '';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Widget _buildNama() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Masukkan nama...",
        labelText: "Nama",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      keyboardType: TextInputType.url,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Nama tidak boleh kosong.';
        }
        return null;
      },
      onSaved: (String? value) {
        nama = value!;
      },
    );
  }

  Widget _buildAlamat() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Masukkan Alamat...",
        labelText: "Alamat",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Alamat tidak boleh kosong.';
        }
        return null;
      },
      onSaved: (String? value) {
        alamat = value!;
      },
    );
  }

  Widget _buildJumlah() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Masukkan Jumlah...",
        labelText: "Jumlah",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Jumlah tidak boleh kosong.';
        }
        return null;
      },
      onSaved: (String? value) {
        jumlah = value!;
      },
    );
  }


  Widget _buildNomor() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Masukkan Nomor Telepon...",
        labelText: "Nomor Telepon",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Nomor Telepon tidak boleh kosong';
        }
        return null;
      },
      onSaved: (String? value) {
        nomor = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildNama(),
            SizedBox(height: 50),
            _buildAlamat(),
            SizedBox(height: 50),
            _buildJumlah(),
            SizedBox(height: 50),
            _buildNomor(),
            SizedBox(height: 50),

            MaterialButton(
              color: Color.fromARGB(255, 11, 94, 215),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0))),
              child: Text(
                'Buat Jadwal',
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
              padding:
                  const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
              onPressed: () {
                if (!_formKey.currentState!.validate()) {
                  return;
                }
                _formKey.currentState!.save();
                print(nama);
                print(alamat);
                print(jumlah);
                print(nomor);

                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Booked Successful')),
                );
              },
              
            ),
          ],
        ),
      ),
    );
  }
}
